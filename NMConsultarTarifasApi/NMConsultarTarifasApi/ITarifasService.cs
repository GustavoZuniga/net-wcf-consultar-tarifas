﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace NMConsultarTarifasApi
{
    [DataContract]
    public class RequestData
    {
        [DataMember]
        public string fecini { get; set; }
        [DataMember]
        public string fecfin { get; set; }
        [DataMember]
        public string fecdep { get; set; }
        [DataMember]
        public string fecret { get; set; }
        [DataMember]
        public string tipodur { get; set; }
        [DataMember]
        public int diasdur { get; set; }
        [DataMember]
        public string origen { get; set; }
        [DataMember]
        public string destino { get; set; }
        [DataMember]
        public string ititipo { get; set; }
        [DataMember]
        public string clase { get; set; }
        [DataMember]
        public int adts { get; set; }
        [DataMember]
        public int chs { get; set; }
        [DataMember]
        public int infs { get; set; }
    }

    [DataContract]
    public class Histograma
    {
       /* [DataMember]
        public string diadep { get; set; }
        [DataMember]
        public decimal duracion { get; set; }
        [DataMember]
        public string diaret { get; set; }
        [DataMember]
        public decimal tarifa { get; set; }*/
       /* [DataMember]
        public Head head { get; set; }
        [DataMember]
        public Detail detail { get; set; }*/
        [DataMember]
        public ResumenModel resumen { get; set; }
        [DataMember]
        public List<DetalleModel> detalle { get; set; }
    }

    public class Head 
    {
        [DataMember]
        public string fecdep { get; set; }
        [DataMember]
        public string fecret { get; set; }
        [DataMember]
        public string origen { get; set; }
        [DataMember]
        public string destino { get; set; }
        [DataMember]
        public string ititipo { get; set; }
        [DataMember]
        public string clase { get; set; }
        [DataMember]
        public decimal tarifabaja { get; set; }
        [DataMember]
        public decimal tarifaalta { get; set; }
    }
    public class Detail
    {
        [DataMember]
        public string diadep { get; set; }
        [DataMember]
        public decimal duracion { get; set; }
        [DataMember]
        public string diaret { get; set; }
        [DataMember]
        public decimal tarifa { get; set; }
    }

    public class ResponseData
    {
        [DataMember]
        public int id { get; set; }
        [DataMember]
        public string resultado { get; set; }
        [DataMember]
        public int codigo { get; set; }
    }

    public class ResumenModel
    {
        [DataMember]
        public string fecdep { get; set; }
        [DataMember]
        public string fecret { get; set; }
        [DataMember]
        public string origen { get; set; }
        [DataMember]
        public string destino { get; set; }
        [DataMember]
        public string ititipo { get; set; }
        [DataMember]
        public string clase { get; set; }
        [DataMember]
        public decimal tarifabaja { get; set; }
        [DataMember]
        public decimal tarifaalta { get; set; }
        [DataMember]
        public decimal duracion { get; set; }

    }

    public class DetalleModel
    {
        [DataMember]
        public string diadep { get; set; }
        [DataMember]
        public decimal duracion { get; set; }
        [DataMember]
        public string diaret { get; set; }
        [DataMember]
        public string tarifa { get; set; }
    }
    
    [ServiceContract]
    public interface ITarifasService
    {
        [OperationContract]
        [WebGet(UriTemplate = "/tarifas/get?fecini={fecini}&fecfin={fecfin}&fecdep={fecdep}&fecret={fecret}&tipodur={tipodur}&diasdur={diasdur}&origen={origen}&destino={destino}&ititipo={ititipo}&clase={clase}&adts={adts}&chs={chs}&infs={infs}", 
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare)]
        List<Histograma> consultar(string fecini, string fecfin, string fecdep, string fecret,
                             string tipodur, int diasdur, string origen, string destino,
                             string ititipo, string clase, int adts, int chs, int infs);


        [OperationContract]
        [WebGet(UriTemplate = "/tarifas/getall?id={id}",
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare)]
        string Get(int id);

    }
}
