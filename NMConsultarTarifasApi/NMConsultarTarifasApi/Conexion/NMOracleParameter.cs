﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Oracle.DataAccess.Client;

namespace NMConsultarTarifasApi.Conexion
{
    public class NMOracleParameter
    {
        public void AddParameter(OracleCommand pObjCommand, string pStrNomParam,
                                 OracleDbType pObjDataType, object pObjValor,
                                 ParameterDirection pObjParamDirection,
                                 int pIntSize = 0, byte pBytPrecision = 0, byte pBytEscala = 0)
        {
            OracleParameter objParameter = new OracleParameter(pStrNomParam, pObjValor);
            objParameter.OracleDbType = pObjDataType;
            objParameter.Direction = pObjParamDirection;

            if (pIntSize > 0)
                objParameter.Size = pIntSize;

            objParameter.Precision = pBytPrecision;
            objParameter.Scale = pBytEscala;

            pObjCommand.Parameters.Add(objParameter);
            objParameter.Dispose();
            objParameter = null;
        }
    }
}