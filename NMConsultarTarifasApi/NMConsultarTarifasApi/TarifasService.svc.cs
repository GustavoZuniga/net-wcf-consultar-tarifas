﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using NMConsultarTarifasApi.Conexion;
using System.Data;
using System.Data.SqlClient;
using Oracle.DataAccess.Client;
using System.Configuration;
using System.IO;

using System.ServiceModel.Web;
using System.Web.Script.Serialization;
using System.Globalization;

namespace NMConsultarTarifasApi
{
    public class TarifasService : ITarifasService
    {
       private List<Histograma> repo;

       static T DeserializarJSON<T>(string cadJson)
       {
           JavaScriptSerializer serializer = new JavaScriptSerializer();
           return serializer.Deserialize<T>(cadJson);
       }

        public TarifasService()
        {
            repo = new List<Histograma>();
        }

        public List<Histograma> consultar(string fecini, string fecfin, string fecdep, string fecret,
                             string tipodur, int diasdur, string origen, string destino,
                             string ititipo, string clase, int adts, int chs, int infs)
        {
            
            NMOracleParameter objNMOracleParameter = new NMOracleParameter();
            try
            {
                using (OracleConnection objConnection = new OracleConnection(DataConexion.strCnx_WebsOracle))
                {
                    using (OracleCommand objCommand = new OracleCommand())
                    {
                        objCommand.CommandText = "PQ_DFS_BSQ_DATAFILES.SP_PRICERESULT_SEARCH";
                        objCommand.CommandType = CommandType.StoredProcedure;
                        objCommand.Connection = objConnection;

                        objNMOracleParameter.AddParameter(objCommand, "fecini", OracleDbType.Varchar2, fecini, ParameterDirection.Input);
                        objNMOracleParameter.AddParameter(objCommand, "fecfin", OracleDbType.Varchar2, fecfin, ParameterDirection.Input);

                        objNMOracleParameter.AddParameter(objCommand, "fecdep", OracleDbType.Varchar2, fecdep, ParameterDirection.Input);
                        objNMOracleParameter.AddParameter(objCommand, "fecret", OracleDbType.Varchar2, fecret, ParameterDirection.Input);

                        objNMOracleParameter.AddParameter(objCommand, "tipodur", OracleDbType.Varchar2, tipodur, ParameterDirection.Input);
                        objNMOracleParameter.AddParameter(objCommand, "diasdur", OracleDbType.Int32, diasdur, ParameterDirection.Input);

                        objNMOracleParameter.AddParameter(objCommand, "origen", OracleDbType.Varchar2, origen, ParameterDirection.Input);
                        objNMOracleParameter.AddParameter(objCommand, "destino", OracleDbType.Varchar2, destino, ParameterDirection.Input);

                        objNMOracleParameter.AddParameter(objCommand, "ititipo", OracleDbType.Varchar2, ititipo, ParameterDirection.Input);
                        objNMOracleParameter.AddParameter(objCommand, "clase", OracleDbType.Varchar2, clase, ParameterDirection.Input);
                        objNMOracleParameter.AddParameter(objCommand, "adts", OracleDbType.Int32, adts, ParameterDirection.Input);
                        objNMOracleParameter.AddParameter(objCommand, "chs", OracleDbType.Int32, chs, ParameterDirection.Input);
                        objNMOracleParameter.AddParameter(objCommand, "infs", OracleDbType.Int32, infs, ParameterDirection.Input);


                        objNMOracleParameter.AddParameter(objCommand, "curresumen", OracleDbType.RefCursor, DBNull.Value, ParameterDirection.Output);
                        objNMOracleParameter.AddParameter(objCommand, "curdetalle", OracleDbType.RefCursor, DBNull.Value, ParameterDirection.Output);

                        objConnection.Open();

                        if (objConnection.State == ConnectionState.Open) {

                            objCommand.Connection = objConnection;

                            decimal saldo = 0;
                            decimal durac = 0;
                            decimal tarifaalta = 0;
                            decimal tarifabaja = 0;
                            int dur = 0;
                            CultureInfo culture = new CultureInfo("en-US");

                            Histograma jsonObject = new Histograma();

                            using (OracleDataReader objReader = objCommand.ExecuteReader())
                            {

                                while (objReader.Read())
                                {
                                    if (objReader["TARIFABAJA"] != DBNull.Value)
                                    {
                                        tarifabaja = Convert.ToDecimal(objReader["TARIFABAJA"].ToString(), culture);
                                    }
                                    if (objReader["TARIFAALTA"] != DBNull.Value)
                                    {
                                        tarifaalta = Convert.ToDecimal(objReader["TARIFAALTA"].ToString(), culture);
                                    }
                                    if (objReader["DURACION"] != DBNull.Value)
                                    {
                                        dur = Convert.ToInt32(objReader["DURACION"].ToString(), culture);
                                    }

                                    jsonObject.resumen = new ResumenModel()
                                    {
                                        fecdep = objReader["FECDEP"].ToString(),
                                        fecret = objReader["FECRET"].ToString(),
                                        origen = objReader["ORIGEN"].ToString(),
                                        destino = objReader["DESTINO"].ToString(),
                                        ititipo = objReader["ITITIPO"].ToString(),
                                        clase = objReader["CLASE"].ToString(),
                                        tarifabaja = tarifabaja,
                                        tarifaalta = tarifaalta,
                                        duracion = dur
                                    };
                                    
                                }

                                objReader.NextResult();

                                jsonObject.detalle = new List<DetalleModel>();
                                while (objReader.Read())
                                {
                                    //if (objReader["TARIFA"] != DBNull.Value)
                                    //{
                                    //    saldo = Convert.ToDecimal(objReader["TARIFA"].ToString(), culture);
                                    //}
                                    if (objReader["DURACION"] != DBNull.Value)
                                    {
                                        durac = Convert.ToDecimal(objReader["DURACION"].ToString(), culture);
                                    }

                                    jsonObject.detalle.Add(new DetalleModel()
                                    {
                                        diadep = objReader[0].ToString(),
                                        duracion = durac,
                                        diaret = objReader[2].ToString(),
                                        tarifa = objReader[3].ToString()
                                    });

                                }

                                repo.Add(new Histograma() 
                                {
                                    resumen = jsonObject.resumen,
                                    detalle = jsonObject.detalle
                                });
                                
                            }
                        }
                        
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }

            return repo;
        }

        public string Get(int id)
        {
            return "OK";
        }
    }
}
